public class Exersise2_2{
  
  public static void main(String[] args){
    String day = "Thursday";
    int date = 1;
    String month = "February";
    int year = 2018;
    
    System.out.println("American format:");
    System.out.print(day);
    System.out.print(", ");
    System.out.print(month);
    System.out.print(" ");
    System.out.print(date);
    System.out.print(", ");
    System.out.println(year);
    
    System.out.println("European format:");
    System.out.print(day);
    System.out.print(" ");
    System.out.print(date);
    System.out.print(" ");
    System.out.print(month);
    System.out.print(" ");
    System.out.println(year);
    
  }
  
}