public class Exercise2_3{
  
  public static void main(String[] args){
    int hour = 11;
    int minute = 35;
    int second = 41;
    
    int secondsSinceMidnight = hour * 60 * 60;
    secondsSinceMidnight += minute * 60;
    secondsSinceMidnight += second;
    System.out.print("Seconds since midnight: ");
    System.out.println(secondsSinceMidnight);
    
    int secondsToMidnight = (24 * 60 * 60) - secondsSinceMidnight;
    System.out.print("Seconds to midnight: ");
    System.out.println(secondsToMidnight);
    
    int percentagePast = (int) (secondsSinceMidnight / (24.0 * 60 * 60) * 100);
    System.out.print("Percentage past: ");
    System.out.print(percentagePast);
    System.out.println("%");
  }
}