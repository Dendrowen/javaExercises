public class Exercise1_1{
  
  public static void main(String[] args){
    System.out.println("A statement is something that gets executed while a comment is some text to describe your code");
    System.out.println("Portable: The program can be put onto another platform and still run");
    System.out.println("Compile: To compose parts into one");
    System.out.println("Executable: A file with computer instructions. And it is able to do something.");
  }
  
}