public class Exercise1_3{
  
  public static void main(String[] args){
    // generate some simple output
    System.out.println("Hello, World!");
  
  }

  //1: File: /home/sjon/Documents/Programming/java/exercises/Exercise1_3.java  [line: 3]
  //   Error: ';' expected


  //2: File: /home/sjon/Documents/Programming/java/exercises/Exercise1_3.java  [line: 13]
  //   Error: reached end of file while parsing
  
  //3: Static Error: This class does not have a static void main method accepting String[].
  
  //4: java.lang.NullPointerException
  
  //5: Static Error: This class does not have a static void main method accepting String[].
  
  //6: File: /home/sjon/Documents/Programming/java/exercises/Exercise1_3.java  [line: 5]
  //   Error: cannot find symbol
  //   symbol:   variable out
  //   location: class Exercise1_3
  
  //7: File: /home/sjon/Documents/Programming/java/exercises/Exercise1_3.java  [line: 5]
  //   Error: cannot find symbol
  //   symbol:   method Println(java.lang.String)
  //   location: variable out of type java.io.PrintStream
  
  //8; no errors
  
  //9: File: /home/sjon/Documents/Programming/java/exercises/Exercise1_3.java  [line: 5]
  //   Error: not a statement
  
  
}